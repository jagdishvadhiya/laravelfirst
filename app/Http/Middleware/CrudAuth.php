<?php

namespace App\Http\Middleware;

use Closure;

class CrudAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->is('/'))
        {
            if( session()->has("login") )
            {
                return redirect('home');
            }
        }
        if( !session()->has("login") )
        {
            return redirect("login");
        }
        return $next($request);
    }
}
