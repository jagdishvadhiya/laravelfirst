@extends("inc.master")
@section("body")
 
                <div class="row justify-content-center ">
                    <div class="col-12"> <p class="h2 text-center text-primary">Student Edit</p> </div>
                    <div class="col-8">
                        <span class="text-success">{{ \Illuminate\Support\Facades\Session::get("status")  }}</span>
                <span class="text-danger">{{ \Illuminate\Support\Facades\Session::get("error")  }}</span>
                <form method="post" action="{{ route('student.update', ['id'=>$student->id]) }}">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="full_name">Full Name</label>
                        @if ( old("full_name") )
                            @php
                                $full_name = old("full_name");    
                            @endphp
                        @else
                            @php
                                $full_name = $student->full_name;    
                            @endphp
                        @endif
                    <input type="text" name="full_name" class="form-control" id="full_name" aria-describedby="emailHelp" placeholder="Enter Full Name" value="{{  $full_name  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "full_name" )
                               {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>

                    <div class="form-group">
                        <label for="user_name">User Name</label>
                        @if ( old("user_name") )
                        @php
                            $user_name = old("user_name");    
                        @endphp
                        @else
                            @php
                                $user_name = $student->user_name;    
                            @endphp
                        @endif
                    <input type="text" name="user_name" class="form-control" id="user_name" aria-describedby="emailHelp" placeholder="Enter Unique User Name" value="{{ $user_name }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "user_name" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="phone_no">Phone Number</label>
                        @if ( old("phone") )
                        @php
                            $phone = old("phone");    
                        @endphp
                        @else
                            @php
                                $phone = $student->phone;    
                            @endphp
                        @endif
                        <input type="text" name="phone" class="form-control" id="phone_no" aria-describedby="emailHelp" placeholder="Enter Phone Number" value="{{ $phone  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "phone" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="user_email">Email address</label>
                        @if ( old("email") )
                        @php
                            $email = old("email");    
                        @endphp
                        @else
                            @php
                                $email = $student->email;    
                            @endphp
                        @endif
                        <input type="email" name="email" class="form-control" id="user_email" aria-describedby="emailHelp" placeholder="Enter Email address" value="{{ $email }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "email" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    
                    <div class="form-group">
                        <label for="user_status">Status</label>
                        @if ( old("status") )
                        @php
                            $status = old("status");    
                        @endphp
                        @else
                            @php
                                $status = $student->status;    
                            @endphp
                        @endif
                        <select name="status" id="status" class="form-control"  aria-describedby="statusHelp">
                            @if( $status == "active")
                                @php
                                    $active = "selected";
                                    $deactive = "";
                                @endphp
                            @else
                                @php
                                    $active = "";
                                    $deactive = "selected";
                                @endphp
                            @endif
                                <option value="active" {{$active}}>Active</option>
                                <option value="deactive" {{$deactive}}>Deactive</option>
                        </select>
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "status" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="user_pass">Password</label>
                        <input type="password" name="password" class="form-control" id="user_pass" placeholder="Enter Password" >
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "password" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="cnf_pass">Confirm Password</label>
                        <input type="password" name="cnf_pass" class="form-control" id="cnf_pass" placeholder="Enter Confirm Password" >

                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                    </div>
                </div>
           
@endsection


