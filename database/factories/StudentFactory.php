<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'full_name' => $faker->name,
        'user_name' => $faker->unique()->lexify("??????"),
        'phone' => $faker->unique()->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$cWiBsHYrt5h6MjlWTbuuy.vMifST4opNaT8E2MJSyEsLtKQjaZ4R.',
        'activate_token'=>Str::random(20),
        'status'=> 'deactive',
    ];
});
