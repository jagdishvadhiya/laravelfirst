<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center m-2 p-2">
                <h1>Register With Laravel CRUD</h1>
                @php
                    if(session()->get("login"))
                    {
                        $link = route('logout');
                    }else{
                        $link = route('login');
                    }
                @endphp
                <a href={{$link}}><button class="btn btn-primary float-right">{{ session()->get("login")?"Logout":"Login"  }}</button></a>
            </div>
            <div class="col-6 m-auto">
                <span class="text-success">{{ \Illuminate\Support\Facades\Session::get("status")  }}</span>
                <span class="text-danger">{{ \Illuminate\Support\Facades\Session::get("error")  }}</span>
                <form method="post" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                        <label for="full_name">Full Name</label>
                        <input type="text" name="full_name" class="form-control" id="full_name" aria-describedby="emailHelp" placeholder="Enter Full Name" value="{{ old("full_name")  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "full_name" )
                               {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>

                    <div class="form-group">
                        <label for="user_name">User Name</label>
                        <input type="text" name="user_name" class="form-control" id="user_name" aria-describedby="emailHelp" placeholder="Enter Unique User Name" value="{{ old("user_name")  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "user_name" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="phone_no">Phone Number</label>
                        <input type="text" name="phone" class="form-control" id="phone_no" aria-describedby="emailHelp" placeholder="Enter Phone Number" value="{{ old("phone")  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "phone" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="user_email">Email address</label>
                        <input type="email" name="email" class="form-control" id="user_email" aria-describedby="emailHelp" placeholder="Enter Email address" value="{{ old("email")  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "email" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="user_pass">Password</label>
                        <input type="password" name="password" class="form-control" id="user_pass" placeholder="Enter Password" >
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "password" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="cnf_pass">Confirm Password</label>
                        <input type="password" name="cnf_pass" class="form-control" id="cnf_pass" placeholder="Enter Confirm Password" >

                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>


