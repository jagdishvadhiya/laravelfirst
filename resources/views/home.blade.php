@extends("inc.master")
@section("body")

                <div class="row text-center">
                    <div class="col">
                        <span class="text-success h4 ">{{ \Illuminate\Support\Facades\Session::get   ("status")  }}</span>
                        <span class="text-danger h4 ">{{ \Illuminate\Support\Facades\Session::get   ("error")  }}</span>
                       <div class="row">
                           <div class="col-12">
                               <table class="display table table-bordered text-center" id="datatable">
                                   <thead>
                                   <tr>
                                       <td colspan="2">Bulk Action</td>
                                       <td><select name="bulkAction" id="bulkAction" class="form-control">
                                               <option value="">Select Bulk Action</option>
                                               <option value="delete">Delete</option>
                                               <option value="active">Active</option>
                                               <option value="deactive">DeActive</option>
                                           </select></td>
                                   </tr>
                                   <tr>
                                       <th><input type="checkbox" name="all_checked" class="form-control-sm" id="checkAll"></th>
                                       <th>Id</th>
                                       <th>Full Name</th>
                                       <th>User Name</th>
                                       <th>Email</th>
                                       <th>Phone</th>
                                       <th>Status</th>
                                       <th>Actions</th>
                                   </tr>
                                   </thead>
                               </table>
                           </div>
                       </div>
{{--                        {{ $students }}--}}

                    </div>
                </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/home.js') }}" ></script>

@endsection
