$(document).ready(function() {


    var checked = $("input:checkbox#checkAll").is(":checked")?'true':'false';
    console.log(checked);
    $(document).on("click",'#checkAll',function (){

        if($("input#checkAll").is(":checked"))
        {
            var checked = $("input:checkbox#checkAll").is(":checked")?'true':'false';
            console.log(checked);
            $("input:checkbox").prop('checked', true);
        }else{

            $("input:checkbox").prop('checked', false);

        }
    });
    var table = $('#datatable').DataTable({
        processing: true,
        serverSide: true,
        // ajax: "student.list",
        ajax:{
            url: "/home",
            data:{ checkBox:function (d){
                                            return $("input:checkbox#checkAll").is(":checked")?'true':'false';
                                        }
                }
        },
        columns: [
            { data:"checkbox", name:"checkbox", orderable:false, searchable:false},
            {data: 'id', name: 'id'},
            {data: 'full_name', name: 'full_name'},
            {data: 'user_name', name: 'user_name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'status', name: 'status'},
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

    });
    $( document ).on("click",".delete",function (event){
        event.preventDefault();
        var id = $(this).attr( "id" );
        swal({
            title: "Are you sure?",
            text: "Delete This Reccord Permenently!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // var deleteurl = $(this).attr('href');
                    // console.log(deleteurl);
                    window.location.href =$(this).attr('href');

                } else {
                    swal("Your Record is safe!");
                }
            });
        console.log(id);
        // window.location.href = "/student/edit/"+id;
    });
    $(document).on("change","#bulkAction",function(){
        var id = [];
        var action = $(this).val();
        swal({
            title: "Are you sure?",
            text: "Are you sure you want to "+action+" this data?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
                if (willDelete) {
                    // code for the add selected id in variable
                    $('.student_checkbox:checked').each(function(){
                        id.push($(this).val());
                    });
                    if(id.length > 0)
                    {
                        $.ajax({
                            url:'/student/bulkaction',
                            data:{
                                    action:action,
                                    allchecked:function (){
                                                            return $("input:checkbox#checkAll").is(":checked")?'true':'false';
                                                        },
                                    ids:id
                                 },
                            dataType:'json',
                            success:function (resopnse){
                                if(resopnse.status)
                                {
                                    
                                    swal({
                                        text: resopnse.message,
                                        icon: "success",
                                    });
                                
                                }else{
                                    swal({
                                        text: resopnse.message,
                                        icon: "warning",
                                    });
                                }
                                $("input:checkbox").prop('checked', false);
                                table.ajax.reload();
                            }

                        });
                    }else{
                        swal("Please select Atleast one checkbox");
                    }
                } else {

                    swal("Your Record is safe!");
                }
            });
        // if(confirm("Are you sure you want to "+action+" this data?"))
        // {
        //     $('.student_checkbox:checked').each(function(){
        //         id.push($(this).val());
        //     });
        //     if(id.length > 0)
        //     {
        //         $.ajax({
        //             url:"{{ route('ajaxdata.massremove')}}",
        //             method:"get",
        //             data:{id:id},
        //             success:function(data)
        //             {
        //                 alert(data);
        //                 $('#student_table').DataTable().ajax.reload();
        //             }
        //         });
        //     }
        //     else
        //     {
        //         alert("Please select atleast one checkbox");
        //     }
        // }
    });


});
