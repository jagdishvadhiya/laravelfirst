<?php

namespace App\Http\Controllers;

use App\Mail\WelcomMail;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use MongoDB\Driver\Session;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function registerUser(Request $req)
    {
        $rules = [
                    "full_name" => "required",
                    "user_name" => ["required","min:6",Rule::unique('students', 'user_name')],
                    "phone" => "required",
                    "email" => ["required",Rule::unique('students', 'email')],
                    "password" => "required|same:cnf_pass",
                    
                ];
        $msg = [
                    'required' => 'The :attribute field is required',
                    'user_name.unique' => 'This Usr Name is taken by Some One else !!! Please try Another one',
                    'email.unique' => 'This Email Is already taken By Someone Else',
                ];
        if ($req->is('student/edit/*'))
        {
            $rules['status'] = "required";
        }
            $req->validate($rules,$msg);
            $student = new Student();
            $data = $req->input();
            $data['password'] = Hash::make($data['password']);
            $data['activate_token'] = Str::random(20);
            if($data =  $student->create($data) ){
                // dd($data->full_name);
                $sessionData = [
                                    "email" => $data->email,
                                    "user_name" => $data->user_name,
                                    "full_name" => $data->full_name
                                ];
                $req->session()->put( "login" , $sessionData );
                $d = [
                        'full_name'=>$data->full_name,
                        'activation_url'=>$data->activate_token,
                    ];
//                Mail::to($data->email)->send(new WelcomMail($d));
                Mail::to("a@a.com")->send(new WelcomMail($d));

                return redirect('home')->with("status", "Registered Successfully... Please Activat Your Account By Email");
            }else{
                return redirect()->back()->with("error", "Registered Failed Somthing Is Wrong...");

            }

    }
    public function loginUser(Request $req)
    {

//        $student = new Student();
        $req->validate([
            'email' => "required",
            'password' => "required",
        ],[
            "required" => "please enter :attribute "
        ]);

        $data = Student::where('email',$req->input('email'))->first();
        if( $data !== null )
        {

            if( Hash::check( $req->input('password'),  $data['password']) )
            {

                if( $data['activate_token'] != '1' )
                {
                    return Redirect()->back()->with("error", "Please Activate Your Account Check Email...");
                }else{
                    $sessionData = [
                        "email" => $data['email'],
                        "user_name" => $data['user_name'],
                        "full_name" => $data['full_name']
                    ];
                    $req->session()->put( "login" , $sessionData );
                    return Redirect('home')->with("status", "Login Successfully...");
                }
            }else{
                return Redirect('login')->with("error", "Login Failed Password Is Wrong !!!");
            }

        }else{
            return Redirect('login')->with("error", "Login Failed Somthing Is Wrong !!!");
        }
//        return Redirect('home')->with("status", "Login Successfully...");

    }
    protected function logout(Request $request)
    {
        $request->session()->forget('login');
        return redirect("home");
    }
}
