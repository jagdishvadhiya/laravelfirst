<?php

use Facade\FlareClient\Api;
use Illuminate\Support\Facades\Route;
use App\User;
use Illuminate\Support\Facades\Config;
use App\Student;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home/user/activate/{activate_key}',"verifyEmailController@verifyKey");


Route::get('/register', function () {
    return view('register');
});
Route::get('/logout', "LoginController@logout")->name('logout');
Route::get('/login', function () {
    if (!session()->has( 'login' ))
    {
        return view('login');
    }else{
        return redirect('home');
    }
});
Route::post("/register", "LoginController@registerUser")->name('register');
Route::post("/login", "LoginController@loginUser")->name('login');
Route::group(["middleware"=>['crudAuth']],function(){
    Route::get('/', function () {
            // if( session()->has("login") )
            // {
            //     return redirect('home');
            // }
        // return view('login');
    });
    Route::get('/student/genrate',"StudentController@GenrateFakeStudents")->name('student.genrate');
    Route::get('/home', "StudentController@index")->name('student.list');
    Route::get('/student/add/', "StudentController@add");
    Route::get('/student/edit/{id}', "StudentController@edit")->name('student.edit');
    Route::put('/student/update/{id}', "StudentController@update")->name('student.update');
    Route::get('/student/delete/{id}', "StudentController@delete")->name('student.delete');
    Route::get('/student/bulkaction', "StudentController@bulkAction")->name('bulk.action');
});

