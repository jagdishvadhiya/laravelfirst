<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;
use yajra\Datatables\DatatablesServiceProvider;
use MongoDB\Driver\Session;
//use Datatables;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class StudentController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if($request->checkBox !="true")
            {
                $checked="";
//                dd("checked");
            }else{
//                dd("not checked");
                $checked="checked";
            }
            $data = Student::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function($row){
                    if($row->status == "active")
                    {
                        $class = 'badge-success';
                    }else{
                        $class = 'badge-danger';

                    }
                    $btn = '<p  class="badge '.$class.' ">'.$row->status.'</p> ';
                    return $btn;
                })
                ->addColumn('action', function($row){

                    $btn = '<a href="'.route('student.edit',['id'=>$row->id]).'" class="edit btn btn-success btn-sm">Edit</a> <a href="'.route('student.delete',['id'=>$row->id]).'"  id="'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $btn;
                })
                ->addColumn('checkbox',function($row)use($checked){
//
                    return '<input type="checkbox" name="student_checkbox[]" class="student_checkbox form-control-sm" value="'.$row->id.'" id="singleCheck" '.$checked.'>';
                })
                ->rawColumns(['status','action','checkbox'])
                ->make(true);
        }

        return view('home');
//        $students = Student::paginate(10);
//        return view("home",compact('students'));
    }
    public function edit($id)
    {

        $student = Student::find($id);
        return view("admin.student.edit",compact('student'));
    }
    public function add()
    {
        return view("admin.student.add");
    }
    public function update(Request $req, $id)
    {
            $rules = [
                        "full_name" => "required",
                        "user_name" => ["required","min:6"],
                        "phone" => "required",
                        "email" => ["required"],
                        "password" => "same:cnf_pass",
                        "status" => "required",
                     ];
        $messages=[
                        'required' => 'The :attribute field is required',
                        'user_name.unique' => 'This Usr Name is taken by Some One else !!! Please try Another one',
                        'email.unique' => 'This Email Is already taken By Someone Else',
                  ];

        $req->validate($rules,$messages);
        $data = $req->input();
        if( $data['password'] == null )
        {
           unset( $data['password'] );
        }else{
            $data['password'] = Hash::make($data['password']);
        }
        $student = Student::find($id);
        if($student->update($data))
        {
            return redirect("home")->with("status","Record Updated Successfully...");
        }else{
            return redirect()->back()->with("status","Somthing Wrong To Update Record...");
        }

    }
    public function delete($id)
    {
       $student = Student::find($id);
       $student->delete();
       return redirect()->back()->with("status","Record Deleted Successfully");
    }
    public function GenrateFakeStudents($no=50)
    {
        $data = session()->get('login');
        if( $data['email'] == 'admin@admin.admin' )
        {
            factory(Student::class, 30)->create();
             return redirect('home')->with("status","30 Data Genrated Successfully...");
        }else{
            return redirect('home')->with("error","You Are Not Access To Genrate Fake Data...");
        }
    }
    public function bulkAction(Request $request)
    {
        $response=array('status'=>'false','message'=>"no Message");
        $checked = $request->allchecked;
        $ids = $request->ids;
        $action = $request->action;
        if($checked == "true")
        {
            $ids=Student::get('id');

        }
        if(count($ids)>0)
        {
            if($action == 'active' || $action == 'deactive' )
            {
                try {
                    
                    $count = Student::whereIn('id',$ids)
                                        ->update([ 'status'=> $action ]);
                    $response['status']="true";        
                    $response['message']=$count.' Records '.ucfirst($action).' Successfully...';
                } catch (\Illuminate\Database\QueryException $e) {
                    $response['message']="Action Failed...";
                }
            }else{
                try {
                    
                    $count = Student::whereIn('id',$ids)
                                        ->delete();
                    $response['status']="true"; 
                    $response['message']=count($ids).' Records '.ucfirst($action).' Successfully...';
                } catch (\Illuminate\Database\QueryException $e) {
                    $response['message']="Action Failed...";
                } 
            }
            
        }else{
            $response['message']="Please Select Any One Record...";
        }
       

        
        return $response;
    }

}
