@component('mail::message')
{{--Welcome To Our Team Mr./Mrs {{print_r($data)}}--}}
Welcome To Our Team Mr./Mrs {{$data['full_name']}}

Please Active Your Account...

@component('mail::button', ['url' => url('home/user/activate/'.$data['activation_url'])])
Activate
@endcomponent

Thanks,<br>
Jagdish Vadhiya.
@endcomponent
