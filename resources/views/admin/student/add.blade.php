@extends("inc.master")
@section("body")
 
                <div class="row justify-content-center ">
                    <div class="col-12"> <p class="h2 text-center text-primary">Add New Student</p> </div>

                    <div class="col-8 ">
                        <span class="text-success">{{ \Illuminate\Support\Facades\Session::get("status")  }}</span>
                <span class="text-danger">{{ \Illuminate\Support\Facades\Session::get("error")  }}</span>
                <form method="post" action="/register">
                    @csrf
                    <div class="form-group">
                        <label for="full_name">Full Name</label>
                    <input type="text" name="full_name" class="form-control" id="full_name" aria-describedby="emailHelp" placeholder="Enter Full Name" value="{{ old("full_name")  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "full_name" )
                               {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>

                    <div class="form-group">
                        <label for="user_name">User Name</label>
                        <input type="text" name="user_name" class="form-control" id="user_name" aria-describedby="emailHelp" placeholder="Enter Unique User Name" value="{{ old("user_name")  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "user_name" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="phone_no">Phone Number</label>
                        <input type="text" name="phone" class="form-control" id="phone_no" aria-describedby="emailHelp" placeholder="Enter Phone Number" value="{{ old("phone")  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "phone" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="user_email">Email address</label>
                        <input type="email" name="email" class="form-control" id="user_email" aria-describedby="emailHelp" placeholder="Enter Email address" value="{{ old("email")  }}">
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "email" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="user_status">Status</label>
                        <select name="status" id="status" class="form-control"  aria-describedby="statusHelp">
                            <option value="">Select Status</option>
                            @if( old("status") === "active")
                                @php
                                    $active = "selected";
                                    $deactive = "";
                                @endphp
                            @elseif(old("status") === "deactive")
                                @php
                                    $active = "";
                                    $deactive = "selected";
                                @endphp
                            @else
                                @php
                                    $active = "";
                                    $deactive = "";
                                @endphp
                            @endif
                                <option value="active" {{$active}}>Active</option>
                                <option value="deactive" {{$deactive}}>Deactive</option>
                        </select>
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "status" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="user_pass">Password</label>
                        <input type="password" name="password" class="form-control" id="user_pass" placeholder="Enter Password" >
                        <small id="formHelp" class="form-text text-danger ">
                            @error( "password" )
                            {{ ucwords($message) }}
                            @enderror
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="cnf_pass">Confirm Password</label>
                        <input type="password" name="cnf_pass" class="form-control" id="cnf_pass" placeholder="Enter Confirm Password" >

                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                    </div>
                </div>
           
@endsection


